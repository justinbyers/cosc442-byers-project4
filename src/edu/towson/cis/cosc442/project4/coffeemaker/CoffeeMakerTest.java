package edu.towson.cis.cosc442.project4.coffeemaker;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;

/**
 * The Class CoffeeMakerTest.
 * @author Justin
 */
public class CoffeeMakerTest extends TestCase {
	
	/** The test coffeemaker variable cm. */
	private CoffeeMaker cm;
	
	/** The i, i2 test inventories. */
	private Inventory i, i2;
	
	/** The r1 r2 test receipes. */
	private Recipe r1, r2;

	/**
	 * Sets up test coffeemaker, inventories, and recipes
	 */
	@Before
	public void setUp() throws Exception {
		cm = new CoffeeMaker();
		i = cm.checkInventory();
		
		i2 = new Inventory();
		i2.setCoffee(1);
		i2.setMilk(2);
		i2.setSugar(3);
		i2.setChocolate(4);

		r1 = new Recipe();
		r1.setName("Coffee");
		r1.setPrice(50);
		r1.setAmtCoffee(6);
		r1.setAmtMilk(1);
		r1.setAmtSugar(1);
		r1.setAmtChocolate(0);
		
		r2 = new Recipe();
		r2.setName("new Coffee");
		r2.setPrice(25);
		r2.setAmtCoffee(3);
		r2.setAmtMilk(2);
		r2.setAmtSugar(1);
		r2.setAmtChocolate(3);
	}
	
	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		i = null;
		i2 = null;
		cm = null;
		r1 = null;
		r2 = null;
	}


	/**
	 * Test adding basic recipe
	 */
	@Test
	public void testAddRecipe1() {
		assertTrue(cm.canAddRecipe(r1));
	}
	
	/**
	 * Test adding duplicate (invalid)
	 */
	@Test
	public void testCanAddRecipe_DuplicateRecipe(){
		cm.canAddRecipe(r1);
		assertFalse(cm.canAddRecipe(r1));
	}


	/**
	 * Test deleting recipe
	 */
	@Test
	public void testDeleteRecipe1() {
		cm.canAddRecipe(r1);
		assertTrue(cm.canDeleteRecipe(r1));
	} 
	
	/**
	 * Test delete recipe 2 mutant no recipe exists.
	 */
	@Test
	public void testDeleteRecipe2_MutantNoRecipe() {
		cm.canAddRecipe(r1);
		assertFalse(cm.canDeleteRecipe(r2));
	} 

	/**
	 * Test edit recipe 1.
	 */
	@Test
	public void testEditRecipe1() {
		cm.canAddRecipe(r1);
		Recipe newRecipe = new Recipe();
		newRecipe = r1;
		newRecipe.setAmtSugar(2);
		assertTrue(cm.canEditRecipe(r1, newRecipe));
	}
	
	/**
	 * Test if each option can be done from main menu
	 */
	@Test
	public void testCheckOptions0() {
		cm.canAddRecipe(r2);
		cm.canDeleteRecipe(r2);
		cm.canAddRecipe(r1);
		cm.canEditRecipe(r1, r2);
		cm.canAddInventory(3, 3, 3, 3);
		cm.checkInventory();
		cm.makeCoffee(r1, 50);
	}
	
	/**
	 * Test addding to inventory (coffee check)
	 */
	@Test
	public void testAddInventory1_Coffee() {
		cm.canAddInventory(1, 2, 3, 4);
		assertSame(i2.getCoffee(), i.getCoffee());
	}
	
	/**
	 * Test adding to inventory (milk check)
	 */
	@Test
	public void testAddInventory2_Milk() {
		cm.canAddInventory(1, 2, 3, 4);
		assertSame(i2.getMilk(), i.getMilk());
	}
	
	/**
	 * Test adding to inventory (coffee check)
	 */
	@Test
	public void testAddInventory3_Sugar() {
		cm.canAddInventory(1, 2, 3, 4);
		assertSame(i2.getSugar(), i.getSugar());
	}
	
	/**
	 * Test adding to inventory (chocolate check)
	 */
	@Test
	public void testAddInventory4_Chocolate() {
		cm.canAddInventory(1, 2, 3, 4);
		assertSame(i2.getChocolate(), i.getChocolate());
	}	
	
	/**
	 * Test adding inventory and checking if equal
	 */
	@Test
	public void testAddInventory5() {
		cm.canAddInventory(1, 2, 3, 4);
		assertTrue(i2.isInventoryEquals(i));
	}	
	
	/**
	 * Test add inventory 6 mutant boundary.
	 */
	@Test
	public void testAddInventory6_MutantBoundary() {
		assertTrue(cm.canAddInventory(0,0,0,0));
	}
	
	/**
	 * Test add inventory 6 mutant boundary negative amt.
	 */
	@Test
	public void testAddInventory6_NegativeAmt() {
		i.setCoffee(2);
		i.setMilk(3);
		i.setSugar(4);
		i.setChocolate(5);
		cm.canAddInventory(-1, 2, 3, 4); // should default to 0 0 0 0, aka no change
		assertTrue(cm.checkInventory().isInventoryEquals(i)); //does 0,0,0,0 = 0,0,0,0
	}
	
	/**
	 * Test add inventory, check inventory coffee.
	 */
	@Test
	public void testCheckInventory_Coffee() {
		cm.canAddInventory(1, 2, 3, 4);
		assertEquals(i2.getCoffee(), i.getCoffee());
	}
	
	/**
	 * Test add inventory, check inventory milk.
	 */
	@Test
	public void testCheckInventory_Milk() {
		cm.canAddInventory(1, 2, 3, 4);
		assertEquals(i2.getMilk(), i.getMilk());
	}
	
	/**
	 * Test add inventory, check inventory sugar.
	 */
	@Test
	public void testCheckInventory_Sugar() {
		cm.canAddInventory(1, 2, 3, 4);
		assertEquals(i2.getSugar(), i.getSugar());
	}
	
	/**
	 * Test add inventory, check inventory chocolate.
	 */
	@Test
	public void testCheckInventory_Chocolate() {
		cm.canAddInventory(1, 2, 3, 4);
		assertEquals(i2.getChocolate(), i.getChocolate());
	}
	
	/**
	 * Test purchase beverage 1 insufficient amt paid.
	 */
	@Test
	public void testPurchaseBeverage1_InsufficientAmtPaid() {
		i.setCoffee(10);
		i.setMilk(15);
		i.setSugar(20);
		i.setChocolate(25);
		cm.canAddInventory(10, 15, 20, 25);
		cm.canAddRecipe(r2); // r2 = price25, coffee3, milk2, sugar1, chocolate3
		cm.makeCoffee(r2, 1); //paid 1 dollar, wont work
		assertTrue(i.isInventoryEquals(cm.checkInventory()));
	}
	
	/**
	 * Test purchase beverage 2 check inventory items used.
	 */
	@Test
	public void testPurchaseBeverage2_CheckInventoryItemsUsed() {
		i.setCoffee(7);
		i.setMilk(13);
		i.setSugar(19);
		i.setChocolate(22);
		cm.canAddInventory(10, 15, 20, 25);
		cm.canAddRecipe(r2); // r2 = price25, coffee3, milk2, sugar1, chocolate3
		cm.makeCoffee(r2, 25);
		assertTrue(i.isInventoryEquals(cm.checkInventory()));
	}
	
	/**
	 * Test purchase beverage 3 amt paid mutant.
	 */
	@Test
	public void testPurchaseBeverage3_AmtPaidMutant() {
		i.setCoffee(10);
		i.setMilk(5);
		i.setSugar(5);
		i.setChocolate(5);
		cm.canAddInventory(10, 5, 5, 5);
		cm.canAddRecipe(r2); // r2 = price25, coffee3, milk2, sugar1, chocolate3
		
		assertEquals(24, cm.makeCoffee(r2, 24));
	}
	
	/**
	 * Test purchase beverage 4 amt paid mutant.
	 */
	@Test
	public void testPurchaseBeverage4_AmtPaidMutant() {
		i.setCoffee(10);
		i.setMilk(5);
		i.setSugar(5);
		i.setChocolate(5);
		cm.canAddInventory(10, 5, 5, 5);
		cm.canAddRecipe(r2); // r2 = price25, coffee3, milk2, sugar1, chocolate3
		
		assertEquals(0, cm.makeCoffee(r2, 25));
	}
	
	/**
	 * Test purchase beverage 5 inventory mutant coffee.
	 */
	@Test
	public void testPurchaseBeverage5_InventoryMutant_Coffee() {
		cm.canAddInventory(9, 8, 7, 6); //10, 10, 10, 10 total
		cm.canAddRecipe(r2); // r2 = price25, coffee3, milk2, sugar1, chocolate3
		cm.makeCoffee(r2, 25);
		assertEquals(7, cm.checkInventory().getCoffee());
	}
	
	/**
	 * Test purchase beverage 6 inventory mutant milk.
	 */
	@Test
	public void testPurchaseBeverage6_InventoryMutant_Milk() {
		cm.canAddInventory(9, 8, 7, 6); //10, 10, 10, 10 total
		cm.canAddRecipe(r2); // r2 = price25, coffee3, milk2, sugar1, chocolate3
		cm.makeCoffee(r2, 25);
		assertEquals(8, cm.checkInventory().getMilk());
	}
	
	/**
	 * Test purchase beverage 7 inventory mutant sugar.
	 */
	@Test
	public void testPurchaseBeverage7_InventoryMutant_Sugar() {
		cm.canAddInventory(9, 8, 7, 6); //10, 10, 10, 10 total
		cm.canAddRecipe(r2); // r2 = price25, coffee3, milk2, sugar1, chocolate3
		cm.makeCoffee(r2, 25);
		assertEquals(9, cm.checkInventory().getSugar());
	}
	
	/**
	 * Test purchase beverage 8 inventory mutant chocolate.
	 */
	@Test
	public void testPurchaseBeverage8_InventoryMutant_Chocolate() {
		cm.canAddInventory(9, 8, 7, 6); //10, 10, 10, 10 total
		cm.canAddRecipe(r2); // r2 = price25, coffee3, milk2, sugar1, chocolate3
		cm.makeCoffee(r2, 25);
		assertEquals(7, cm.checkInventory().getChocolate());
	}
	
	/**
	 * Test get recipes function
	 */
	@Test
	public void testGetRecipesForName1() {
		cm.canAddRecipe(r1);
		assertTrue(r1.isRecipeEquals(cm.getRecipeForName(r1.getName())));
			
	}
	
}