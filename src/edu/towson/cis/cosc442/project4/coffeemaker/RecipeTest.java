/**
 * 
 */
package edu.towson.cis.cosc442.project4.coffeemaker;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

// TODO: Auto-generated Javadoc
/**
 * The Class RecipeTest.
 *
 * @author Justin
 */
public class RecipeTest {

	/** The test recipe 2. */
	Recipe testRecipe1, testRecipe2;

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		testRecipe1 = new Recipe();

		testRecipe2 = new Recipe();
		testRecipe2.setName("");
		testRecipe2.setPrice(50);
		testRecipe2.setAmtCoffee(5);
		testRecipe2.setAmtMilk(6);
		testRecipe2.setAmtSugar(7);
		testRecipe2.setAmtChocolate(8);

	}

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		testRecipe1 = null;
		testRecipe2 = null;
	}

	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#getAmtChocolate()}.
	 */
	@Test
	public final void testGetAmtChocolate1() {
		assertEquals(8, testRecipe2.getAmtChocolate(), 0.001);
	}

	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#setAmtChocolate(int)}.
	 */
	@Test
	public final void testSetAmtChocolate1() {
		testRecipe2.setAmtChocolate(10);
		assertEquals(10, testRecipe2.getAmtChocolate(), 0.001);
	}

	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#setAmtChocolate(int)}.
	 */
	@Test
	public final void testSetAmtChocolate2() {
		testRecipe2.setAmtChocolate(-1);
		assertEquals(0, testRecipe2.getAmtChocolate(), 0.001);
	}

	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#setAmtChocolate(int)}.
	 */
	@Test
	public final void testSetAmtChocolate3() {
		testRecipe2.setAmtChocolate(0);
		assertEquals(0, testRecipe2.getAmtChocolate(), 0.001);
	}

	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#setAmtChocolate(int)}.
	 */
	@Test
	public final void testSetAmtChocolate4() {
		testRecipe2.setAmtChocolate(1);
		assertEquals(1, testRecipe2.getAmtChocolate(), 0.001);
	}
	
	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#getAmtCoffee()}.
	 */
	@Test
	public final void testGetAmtCoffee() {
		assertEquals(5, testRecipe2.getAmtCoffee(), 0.001);
	}

	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#setAmtCoffee(int)}.
	 */
	@Test
	public final void testSetAmtCoffee1() {
		testRecipe1.setAmtCoffee(10);
		assertNotNull(testRecipe1.getAmtCoffee());
	}

	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#setAmtCoffee(int)}.
	 */
	@Test
	public final void testSetAmtCoffee2() {
		testRecipe2.setAmtCoffee(1);
		assertEquals(1, testRecipe2.getAmtCoffee(), 0.001);
	}
	
	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#setAmtCoffee(int)}.
	 */
	@Test
	public final void testSetAmtCoffee3() {
		testRecipe2.setAmtCoffee(-1);
		assertEquals(0, testRecipe2.getAmtCoffee(), 0.001);
	}
	
	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#setAmtCoffee(int)}.
	 */
	@Test
	public final void testSetAmtCoffee4() {
		testRecipe2.setAmtCoffee(0);
		assertEquals(0, testRecipe2.getAmtCoffee(), 0.001);
	}
	
	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#getAmtMilk()}.
	 */
	@Test
	public final void testGetAmtMilk1() {
		testRecipe2.setAmtMilk(20);
		assertEquals(20, testRecipe2.getAmtMilk(), 0.001);
	}

	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#setAmtMilk(int)}.
	 */
	@Test
	public final void testSetAmtMilk1() {
		testRecipe1.setAmtMilk(99);
		assertNotNull(testRecipe1.getAmtMilk());
	}

	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#setAmtMilk(int)}.
	 */
	@Test
	public final void testSetAmtMilk2() {
		testRecipe2.setAmtMilk(1);
		assertEquals(1, testRecipe2.getAmtMilk(), 0.001);
	}

	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#setAmtMilk(int)}.
	 */
	@Test
	public final void testSetAmtMilk3() {
		testRecipe2.setAmtMilk(-1);
		assertEquals(0, testRecipe2.getAmtMilk(), 0.001);
	}

	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#setAmtMilk(int)}.
	 */
	@Test
	public final void testSetAmtMilk4() {
		testRecipe2.setAmtMilk(0);
		assertEquals(0, testRecipe2.getAmtMilk(), 0.001);
	}
	
	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#getAmtSugar()}.
	 */
	@Test
	public final void testGetAmtSugar1() {
		testRecipe2.setAmtSugar(24);
		assertEquals(24, testRecipe2.getAmtSugar(), 0.001);
	}

	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#setAmtSugar(int)}.
	 */
	@Test
	public final void testSetAmtSugar1() {
		testRecipe1.setAmtSugar(1);
		assertNotNull(testRecipe1.getAmtSugar());
	}
	
	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#setAmtSugar(int)}.
	 */
	@Test
	public final void testSetAmtSugar2() {
		testRecipe2.setAmtSugar(1);
		assertEquals(1, testRecipe2.getAmtSugar(), 0.001);
	}
	
	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#setAmtSugar(int)}.
	 */
	@Test
	public final void testSetAmtSugar3() {
		testRecipe2.setAmtSugar(-1);
		assertEquals(0, testRecipe2.getAmtSugar(), 0.001);
	}
	
	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#setAmtSugar(int)}.
	 */
	@Test
	public final void testSetAmtSugar4() {
		testRecipe2.setAmtSugar(0);
		assertEquals(0, testRecipe2.getAmtSugar(), 0.001);
	}
	
	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#getName()}.
	 */
	@Test
	public final void testGetName1() {
		testRecipe2.setName("Coffee??");
		assertEquals("Coffee??", testRecipe2.getName());
	}

	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#setName(java.lang.String)}.
	 */
	@Test
	public final void testSetName1() {
		testRecipe1.setName("AAA");
		assertNotNull(testRecipe1.getName());
	}
	
	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#setName(java.lang.String)}.
	 */
	@Test
	public final void testSetName2() {
		testRecipe2.setName("AA");
		assertEquals("AA", testRecipe2.getName());
	}

	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#getPrice()}.
	 */
	@Test
	public final void testGetPrice1() {
		testRecipe2.setPrice(13);
		assertEquals(13, testRecipe2.getPrice(), 0.001);
	}
	
	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#setPrice(int)}.
	 */
	@Test
	public final void testSetPrice1() {
		testRecipe1.setPrice(99);
		assertNotNull(testRecipe1.getPrice());
	}
	
	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#setPrice(int)}.
	 */
	@Test
	public final void testSetPrice2() {
		testRecipe2.setPrice(1);
		assertEquals(1, testRecipe2.getPrice(), 0.001);
	}
	
	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#setPrice(int)}.
	 */
	@Test
	public final void testSetPrice3() {
		testRecipe2.setPrice(-1);
		assertEquals(0, testRecipe2.getPrice(), 0.001);
	}
	
	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#setPrice(int)}.
	 */
	@Test
	public final void testSetPrice4() {
		testRecipe2.setPrice(0);
		assertEquals(0, testRecipe2.getPrice(), 0.001);
	}

	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#isRecipeEquals(edu.towson.cis.cosc442.project4.coffeemaker.Recipe)}.
	 */
	@Test
	public final void testIsRecipeEquals() {
		testRecipe2.setName("testname123");
		
		Recipe testRecipe3 = new Recipe();
		testRecipe3.setName("testname123");

		assertTrue(testRecipe2.isRecipeEquals(testRecipe3));
	}
	
	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#isRecipeEquals(edu.towson.cis.cosc442.project4.coffeemaker.Recipe)}.
	 */
	@Test
	public final void testIsRecipeEquals_Name1Null() {
		Recipe testRecipe3 = new Recipe();
		assertFalse(testRecipe3.isRecipeEquals(testRecipe2));
	}
	
	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#isRecipeEquals(edu.towson.cis.cosc442.project4.coffeemaker.Recipe)}.
	 */
	@Test
	public final void testIsRecipeEquals_Name2Null() {
		Recipe testRecipe3 = new Recipe();
		assertFalse(testRecipe2.isRecipeEquals(testRecipe3));
	}
	
	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#isRecipeEquals(edu.towson.cis.cosc442.project4.coffeemaker.Recipe)}.
	 */
	@Test
	public final void testIsRecipeEquals_NamesNotEqual() {
		Recipe testRecipe3 = new Recipe();
		testRecipe3.setName("AAA");
		assertFalse(testRecipe2.isRecipeEquals(testRecipe3));
	}
	
	/**
	 * Test method for
	 * {@link edu.towson.cis.cosc442.project4.coffeemaker.Recipe#toString(edu.towson.cis.cosc442.project4.coffeemaker.Recipe)}.
	 */
	@Test
	public final void testToString() {
		Recipe testRecipe3 = new Recipe();
		testRecipe3.setName("AAA");
		assertEquals("AAA", testRecipe3.toString());
	}
	
//	/**
//	 * Test set price price less than zero error
//	 */
//	@Test(expected = VendingMachineException.class)
//	public final void testSetPrice_PriceLessThanZero() {
//		item1.setPrice(-1.00); //check items cant be set to negative price
//	}

}
