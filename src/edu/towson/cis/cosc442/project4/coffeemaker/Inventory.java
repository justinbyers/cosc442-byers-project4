package edu.towson.cis.cosc442.project4.coffeemaker;

/**
 * Inventory for the coffee maker.
 *
 * @author Josh
 * @version $Revision: 1.0 $
 */
public class Inventory {
    
    /** The Coffee variable */
    private static int Coffee;
    
    /** The Milk variable */
    private static int Milk;
    
    /** The Sugar variable */
    private static int Sugar;
    
    /** The Chocolate variable */
    private static int Chocolate;
    
    /**
     * Instantiates a new inventory.
     */
    public Inventory() {
    	Inventory.Coffee = 15;
    	Inventory.Milk = 15;
    	Inventory.Sugar = 15;
    	Inventory.Chocolate = 15;
    }
    
    /**
     * Method getChocolate in inventory.
     * @return int chocolate
     */
    public int getChocolate() {
        return Chocolate;
    }
    /**
     * Method setChocolate.
     * @param chocolate int
     */
    public void setChocolate(int chocolate) {
    	if(chocolate >= 1) {
    		Inventory.Chocolate = chocolate;
    	}
    	else {
    		Inventory.Chocolate = 0;
    	}
        
    }
    /**
     * Method getCoffee in inventory.
     * @return int coffee
     */
    public int getCoffee() {
        return Coffee;
    }
    /**
     * Method setCoffee.
     * @param coffee int
     */
    public void setCoffee(int coffee) {
    	if(coffee >= 1) {
    		Inventory.Coffee = coffee;
    	}
    	else {
    		Inventory.Coffee = 0;
    	}
    }
    /**
     * Method getMilk in inventory.
     * @return int milk
     */
    public int getMilk() {
        return Milk;
    }
    /**
     * Method setMilk.
     * @param milk int
     */
    public void setMilk(int milk) {
    	if(milk >= 1) {
    		Inventory.Milk = milk;
    	}
    	else {
    		Inventory.Milk = 0;
    	}
    }
    /**
     * Method getSugar in inventory 
     * @return int
     */
    public int getSugar() {
        return Sugar;
    }
    /**
     * Method setSugar.
     * @param sugar int
     */
    public void setSugar(int sugar) {
    	if(sugar >= 1) {
    		Inventory.Sugar = sugar;
    	}
    	else {
    		Inventory.Sugar = 0;
    	}
    }
    
    /**
     * Returns true if there are enough ingredients to make
     * the beverage.
     *
     * @param r the r
     * @return boolean
     */
    public boolean areEnoughIngredients(Recipe r) {
        boolean isEnough = true;
        if(Inventory.Coffee < r.getAmtCoffee()) {
            isEnough = false;
        }
        if(Inventory.Milk < r.getAmtMilk()) {
            isEnough = false;
        }
        if(Inventory.Sugar < r.getAmtSugar()) {
            isEnough = false;
        }
        if(Inventory.Chocolate < r.getAmtChocolate()) {
            isEnough = false;
        }
        return isEnough;
    }
    
    /**
     * Method toString.
     * @return String
     */
    public String toString() {
    	return "Coffee: " + getCoffee() + System.getProperty("line.separator") +
			"Milk: " + getMilk() + System.getProperty("line.separator") + 
			"Sugar: " + getSugar() + System.getProperty("line.separator") +
			"Chocolate: " + getChocolate() + System.getProperty("line.separator") ;
    }
    
    /**
     * Method isInventoryEquals to another inventory
     * @param i Inventory
     * @return boolean
     */
    public boolean isInventoryEquals(Inventory i) {
    	if(this.getCoffee() == i.getCoffee()
    			&& this.getMilk() == i.getMilk()
    			&& this.getSugar() == i.getSugar()
    			&& this.getChocolate() == i.getChocolate()) {
    		return true;
    	}
        return false;
    }
}
