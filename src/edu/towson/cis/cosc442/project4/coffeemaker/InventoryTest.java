/**
 * 
 */
package edu.towson.cis.cosc442.project4.coffeemaker;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The Class InventoryTest to test Inventory.java.
 *
 * @author Justin
 */
public class InventoryTest {

	/** The i, i2 test inventory variables. */
	Inventory i, i2;
	
	/** The r1, r2 test Recipe variables. */
	Recipe r1, r2;
	
	/**
	 * Sets up test inventories and recipes.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		i = new Inventory();
		i2 = new Inventory();
		i2.setCoffee(5);
		i2.setMilk(10);
		i2.setSugar(15);
		i2.setChocolate(20);
		
		r1 = new Recipe();
		r1.setName("r1recipe");
		r1.setPrice(50);
		r1.setAmtCoffee(2);
		r1.setAmtMilk(3);
		r1.setAmtSugar(4);
		r1.setAmtChocolate(5);
		
		r2 = new Recipe();
		r2.setName("r2recipe");
		r2.setPrice(1000);
		r2.setAmtCoffee(55);
		r2.setAmtMilk(66);
		r2.setAmtSugar(77);
		r2.setAmtChocolate(88);
		
	}

	/**
	 * Tear down to set test variables to null.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		i = null;
		i2 = null;
		r1 = null;
		r2 = null;
	}

	/** 
	 * Test method for {@link edu.towson.cis.cosc442.project4.coffeemaker.Inventory#Inventory()}.
	 * Tests if inventory exists before any other tests are done
	 */
	@Test
	public final void testInventory1_InventoryExists() {
		assertNotNull(i);
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project4.coffeemaker.Inventory#getChocolate()}.
	 * Tests if .getChocolate returns correct value
	 */
	@Test
	public final void testGetChocolate() {
		assertEquals(20, i2.getChocolate(), 0.001);
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project4.coffeemaker.Inventory#setChocolate(int)}.
	 * Tests if .setChocolate sets correct value
	 * uses empty inventory so any return value indicates change
	 */
	@Test
	public final void testSetChocolate1() {
		i.setChocolate(1);
		assertEquals(1, i.getChocolate(), 0.001);
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project4.coffeemaker.Inventory#setChocolate(int)}.
	 * Tests .setChocolate if negative value is used
	 */
	@Test
	public final void testSetChocolate2_NegativeAmt() {
		i.setChocolate(-1);
		assertEquals(0, i.getChocolate(), 0.001);
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project4.coffeemaker.Inventory#getCoffee()}.
	 * Tests getCoffee
	 */
	@Test
	public final void testGetCoffee() {
		assertEquals(5, i2.getCoffee(), 0.001);
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project4.coffeemaker.Inventory#setCoffee(int)}.
	 * Tests setCoffee on empty inventory
	 */
	@Test
	public final void testSetCoffee1() {
		i.setCoffee(1);
		assertEquals(1, i.getCoffee(), 0.001);
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project4.coffeemaker.Inventory#setCoffee(int)}.
	 * Tests negative setCoffee equates to 0 (default)
	 */
	@Test
	public final void testSetCoffee2_NegativeAmt() {
		i.setCoffee(-1);
		assertEquals(0, i.getCoffee(), 0.001);
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project4.coffeemaker.Inventory#getMilk()}.
	 * Tests getMilk
	 */
	@Test
	public final void testGetMilk() {
		assertEquals(10, i2.getMilk(), 0.001);
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project4.coffeemaker.Inventory#setMilk(int)}.
	 * Tests setMilk boundary test
	 */
	@Test
	public final void testSetMilk1() {
		i.setMilk(1);
		assertEquals(1, i.getMilk(), 0.001);
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project4.coffeemaker.Inventory#setMilk(int)}.
	 * Tests setMilk negative boundary test
	 */
	@Test
	public final void testSetMilk2_NegativeAmt() {
		i.setMilk(-1);
		assertEquals(0, i.getMilk(), 0.001);
	}
	
	/**
	 * Test method for {@link edu.towson.cis.cosc442.project4.coffeemaker.Inventory#getSugar()}.
	 * Tests getSugar
	 */
	@Test
	public final void testGetSugar() {
		assertEquals(15, i2.getSugar(), 0.001);
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project4.coffeemaker.Inventory#setSugar(int)}.
	 * Tests setSugar boundary test 
	 */
	@Test
	public final void testSetSugar1() {
		i.setSugar(1);
		assertEquals(1, i.getSugar(), 0.001);
	}
	
	/**
	 * Test method for {@link edu.towson.cis.cosc442.project4.coffeemaker.Inventory#setSugar(int)}.
	 * Tests setSugar negative boundary test
	 */
	@Test
	public final void testSetSugar2_NegativeAmt() {
		i.setSugar(-1);
		assertEquals(0, i.getSugar(), 0.001);
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project4.coffeemaker.Inventory#areEnoughIngredients(edu.towson.cis.cosc442.project4.coffeemaker.Recipe)}.
	 * Tests if method returns false, implying there ARENT enough ingredients (coffee)
	 */
	@Test
	public final void testAreEnoughIngredients1_Coffee() {
		assertFalse(i2.areEnoughIngredients(r2)); //insufficient coffee
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project4.coffeemaker.Inventory#areEnoughIngredients(edu.towson.cis.cosc442.project4.coffeemaker.Recipe)}.
	 * Tests if method returns false, implying there ARENT enough ingredients (milk)
	 */
	@Test
	public final void testAreEnoughIngredients2_Milk() {
		i2.setCoffee(999);
		assertFalse(i2.areEnoughIngredients(r2)); //insufficient milk
	}
	
	/**
	 * Test method for {@link edu.towson.cis.cosc442.project4.coffeemaker.Inventory#areEnoughIngredients(edu.towson.cis.cosc442.project4.coffeemaker.Recipe)}.
	 * Tests if method returns false, implying there ARENT enough ingredients (sugar)
	 */
	@Test
	public final void testAreEnoughIngredients3_Sugar() {
		i2.setCoffee(999);
		i2.setMilk(999);
		assertFalse(i2.areEnoughIngredients(r2)); //insufficient sugar
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project4.coffeemaker.Inventory#areEnoughIngredients(edu.towson.cis.cosc442.project4.coffeemaker.Recipe)}.
	 * Tests if method returns false, implying there ARENT enough ingredients (Chocolate)
	 */
	@Test
	public final void testAreEnoughIngredients4_Chocolate() {
		i2.setCoffee(999);
		i2.setMilk(999);
		i2.setSugar(999);
		assertFalse(i2.areEnoughIngredients(r2)); //insufficient chocolate
	}
	
	/**
	 * Test method for {@link edu.towson.cis.cosc442.project4.coffeemaker.Inventory#areEnoughIngredients(edu.towson.cis.cosc442.project4.coffeemaker.Recipe)}.
	 * Test if there are enough ingredients (exactly enough)
	 */
	@Test
	public final void testAreEnoughIngredients5() {
		i2.setCoffee(55);
		i2.setMilk(66);
		i2.setSugar(77);
		i2.setChocolate(88);
		assertTrue(i2.areEnoughIngredients(r2));
	}
	
	/**
	 * Test method for {@link edu.towson.cis.cosc442.project4.coffeemaker.Inventory#areEnoughIngredients(edu.towson.cis.cosc442.project4.coffeemaker.Recipe)}.
	 * test enough ingredients (more than enough)
	 */
	@Test
	public final void testAreEnoughIngredients6() {
		i2.setCoffee(999);
		i2.setMilk(999);
		i2.setSugar(999);
		i2.setChocolate(999);
		assertTrue(i2.areEnoughIngredients(r2));
	}
	
	/**
	 * Test if inventories are equal
	 */
	@Test
	public final void testIsInventoryEquals1() {
		i.setCoffee(5);
		i.setMilk(10);
		i.setSugar(15);
		i.setChocolate(20);
		assertTrue(i.isInventoryEquals(i2));
	}
	
	/**
	 * Test if inventories are NOT equal
	 */
	@Test
	public final void testIsInventoryEquals2_NotEqual() {
		i.setCoffee(5);
		i.setMilk(0);
		i.setSugar(0);
		i.setChocolate(0);
		assertTrue(i.isInventoryEquals(i2)); // 5,0,0,0 != 5,10,15,20
	}
	
	/**
	 * Test string output
	 */
	@Test
	public final void testToString() {
		String output = "Coffee: " + i2.getCoffee() + System.getProperty("line.separator") +
				"Milk: " + i2.getMilk() + System.getProperty("line.separator") +
				"Sugar: " + i2.getSugar() + System.getProperty("line.separator") +
				"Chocolate: " + i2.getChocolate() + System.getProperty("line.separator");
		assertEquals(output, i2.toString());
	}

}
